Para mim, Visão Computação são conjunto de técnicas para se extrair informações sobre análise de determinada imagem. Informações estas inseridas
dentro de um domínio de problema pré estabelecido.

Este programa é um protótipo que criei para explorar os recursos da biblioteca da Intel OpenCV. E, o domínio do problema é reconhecimento de pessoas
em um ambiente controlado. No caso, as pessoas querem ser identificadas e aceitam seu pré-cadastro, assim, para um controle de acessso, por exemplo,
o sistema entra em funcionamento para localizar as pessoas.

Embutido no projeto já contém:
https://github.com/Laex/Delphi-OpenCV
https://docs.opencv.org/2.4.13.6/

Desenvolvi o protótipo usando o Delphi Tokyo, com o Sql Server.
O script para criação das 2 tabelas está em anexo.
O driver do banco que usei está no endereço https://www.devart.com/dbx/sqlserver/

Obrigado

Marcio Cruz - programador@marciofcruz.com